package com.odifek.hello

internal data class Hello(val message: String = "Hello from AWS codeartifact\nWe now build and deploy this using pipelines. Cool :)")
